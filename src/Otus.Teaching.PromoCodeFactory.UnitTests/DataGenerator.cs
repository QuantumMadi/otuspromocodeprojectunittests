using AutoFixture;

namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
    public static class DataGenerator<T>
    {
        private readonly static Fixture _fixture;
        static DataGenerator()
        {
            _fixture = new Fixture();
            _fixture.OmitAutoProperties = true;
        }

        public static T GetFreezedItem(T partner)
            => _fixture.Freeze<T>(x => x.FromSeed(x => x = partner));

        public static T GetCreatedItem() 
            => _fixture.Create<T>();        
    }
}