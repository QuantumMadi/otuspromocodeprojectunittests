using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public static class InMemoryDatabaseOptionsBuilder<T>
        where T : DbContext
    {
        private static readonly DbContextOptions<T> _optionsBuilder;
        static InMemoryDatabaseOptionsBuilder() =>
             _optionsBuilder = new DbContextOptionsBuilder<T>()
             .UseInMemoryDatabase("Fake_Database").Options;
        public static DbContextOptions<T> GetDbContextOptions() =>
             _optionsBuilder;
    }
}