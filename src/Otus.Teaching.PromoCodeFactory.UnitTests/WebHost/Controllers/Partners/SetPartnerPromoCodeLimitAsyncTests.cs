﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly PartnersController _partnersController;
        private readonly Mock<IRepository<Partner>> _repositoryMock;
        private readonly IFixture _fixture;
        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _repositoryMock = _fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = _fixture.Build<PartnersController>()
                .OmitAutoProperties().Create();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerNotFound_404BadRequest()
        {
            //Given
            var partnerId = DataGenerator<Guid>.GetCreatedItem();
            var partner = DataGenerator<Partner>.GetFreezedItem(null);
            var request = DataGenerator<SetPartnerPromoCodeLimitRequest>.GetFreezedItem(null);
            _repositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
               .ReturnsAsync(partner);

            //When
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Then
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerBlocked_400BadRquest()
        {
            //Given
            var id = Guid.NewGuid();
            var request = DataGenerator<SetPartnerPromoCodeLimitRequest>.GetFreezedItem(null);
            var partner = DataGenerator<Partner>.GetFreezedItem(new Partner
            {
                IsActive = false
            });

            _repositoryMock.Setup(x => x.GetByIdAsync(id))
                .ReturnsAsync(partner);

            //When
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(id, request);

            //Then
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimits_OffPreviousLimit_And_NewLimitGraterThenNull_And_NumberIssuedPromoCodesGreaterThenNull()
        {
            //Given
            var request = DataGenerator<SetPartnerPromoCodeLimitRequest>
                   .GetFreezedItem(new SetPartnerPromoCodeLimitRequest
                   {
                       Limit = 10,
                   });
            var partnerId = Guid.NewGuid();
            var limitId = Guid.NewGuid();
            var partner = DataGenerator<Partner>.GetFreezedItem(new Partner
            {
                Id = partnerId,
                IsActive = true,
                NumberIssuedPromoCodes = 10,
                PartnerLimits = new List<PartnerPromoCodeLimit>{
                    new PartnerPromoCodeLimit{
                        Id = limitId,
                        PartnerId = partnerId,
                        Limit = 10,
                    }
                }
            });
            _repositoryMock.Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            //When
            var result = await _partnersController
                .SetPartnerPromoCodeLimitAsync(partnerId, request);
            //Then
            partner.NumberIssuedPromoCodes.Should().Be(0);
            partner.PartnerLimits.First(x => x.Id == limitId).CancelDate.Should().NotBeNull();
            partner.PartnerLimits.Last().Limit.Should().BeGreaterThan(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetLimits_CheckLimitInDatabase()
        {
            //Given
            var options = InMemoryDatabaseOptionsBuilder<DataContext>
                .GetDbContextOptions();
            var request = DataGenerator<SetPartnerPromoCodeLimitRequest>
                      .GetFreezedItem(new SetPartnerPromoCodeLimitRequest
                      {
                          Limit = 5,
                      });
            var partnerId = Guid.NewGuid();
            var limitId = Guid.NewGuid();
            var partner = DataGenerator<Partner>.GetFreezedItem(new Partner
            {
                Id = partnerId,
                IsActive = true,
                NumberIssuedPromoCodes = 10,
                PartnerLimits = new List<PartnerPromoCodeLimit>{
                    new PartnerPromoCodeLimit{
                        Id = limitId,
                        Limit = 10,
                    }
                }
            });            
            var partnerForContext = DataGenerator<Partner>.GetFreezedItem(new Partner{
                Id = partnerId,
                NumberIssuedPromoCodes = 10
            });

            //When
            _repositoryMock.Setup(x => x.GetByIdAsync(partnerId))
              .ReturnsAsync(partner);
            //Then

            using (var context = new DataContext(options))
            {
                var EfRepository = new EfRepository<Partner>(context);
                await EfRepository.AddAsync(partnerForContext);
                context.Entry<Partner>(partnerForContext).State = EntityState.Detached;
                await context.SaveChangesAsync(); 

                _repositoryMock.Setup(x => x.UpdateAsync(partner))
                    .Callback(() =>
                    {
                        EfRepository.UpdateAsync(partner);                        
                    });

                var result =  await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
                var partnerResultFromDb = await context.Partners.FirstAsync(x => x.Id == partnerId);
                
                partnerResultFromDb.PartnerLimits.First(x => x.Limit == 5).Should().NotBeNull();
            }
        }
    }
}