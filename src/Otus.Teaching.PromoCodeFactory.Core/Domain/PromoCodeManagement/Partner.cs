﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Partner
        : BaseEntity, IPartner
    {
        public string Name { get; set; }

        public int NumberIssuedPromoCodes  { get; set; }

        public bool IsActive { get; set; }

        public virtual ICollection<PartnerPromoCodeLimit> PartnerLimits { get; set; }
    }
}