using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services
{
    public interface ISuggestions
    {
        void SetPartner(IPartner partner);   
        void SetLimit(object limit);
    }
}