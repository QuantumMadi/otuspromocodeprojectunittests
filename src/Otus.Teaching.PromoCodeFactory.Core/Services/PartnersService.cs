using System;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Services
{
    public static class PartnersService
    {
        public static void SetPartnerLimit(Partner partner)
        {
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
               !x.CancelDate.HasValue);

            if (activeLimit != null)
            {              
                partner.NumberIssuedPromoCodes = 0;               
                activeLimit.CancelDate = DateTime.Now;
            }
        }
    }
}