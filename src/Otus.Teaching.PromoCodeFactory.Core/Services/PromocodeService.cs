using System;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Services
{
    public class PromocodeLimitService : ISuggestions
    {
        public PartnerPromoCodeLimit PartnerPromocode { get; }
        public PromocodeLimitService()
        {
            PartnerPromocode = new PartnerPromoCodeLimit();
            PartnerPromocode.CreateDate = DateTime.Now;
        }
        public void SetPartner(IPartner partner)
        {
            PartnerPromocode.Partner = partner as Partner;
            PartnerPromocode.PartnerId = (partner as Partner).Id;
        }
        public void SetLimit(object limit)
        {
            if (int.TryParse(limit.ToString(), out var valuableLimit))
            {
                PartnerPromocode.Limit = valuableLimit;
            }
            else
            {
                throw new ArgumentException("Provide valuable limit");
            }
        }
        public void SetEndDate(DateTime endDate)
        {
            PartnerPromocode.EndDate = endDate;
        }
    }
}